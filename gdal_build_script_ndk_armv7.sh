#!/bin/sh

# 说明：
# 本脚本适用于在Ubuntu 22.04中全自动交叉编译适用于Android ArmV7的sqlite3、proj和gdal库。
# 部分网络慢的问题，在脚本中已注释了手动下载的方案。
# 本脚本是基于gdal库本身的脚本，修改了部分错误后形成的全自动脚本。

# 使用方法：
# chmod a+x gdal_bulid_script_ndk_armv7.sh
# sudo ./gdal_bulid_script_ndk_armv7.sh
# 编译结束后，gdal arm的库文件在 /tmp/gdal_ndk_arm 目录中

set -e

echo "install tools..."
apt-get update -y
# pkg-config sqlite3 for proj compilation
DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    wget unzip ccache curl ca-certificates \
    pkg-config make binutils sqlite3 \
    automake

export WORK_PATH=$(realpath ${BASH_SOURCE-$0}|xargs dirname)/build_Armv7

if [ ! -d "$WORK_PATH" ];then
  mkdir -p $WORK_PATH
fi
cd $WORK_PATH

# We need a recent cmake for recent NDK versions
echo "setup cmake..."
if [ ! -f "cmake-3.22.3-linux-x86_64.tar.gz" ];then
# 如果下载不下来，可以手动下载后放到build_Armv7目录
  wget -c https://github.com/Kitware/CMake/releases/download/v3.22.3/cmake-3.22.3-linux-x86_64.tar.gz
fi

if [ ! -d "cmake-3.22.3-linux-x86_64" ];then
  tar xzf cmake-3.22.3-linux-x86_64.tar.gz
fi

export PATH=$PWD/cmake-3.22.3-linux-x86_64/bin:$PATH

# Download Android NDK
echo "setup Android NDK..."
if [ ! -f "android-ndk-r23b-linux.zip" ];then
# 如果下载不下来，可以手动下载后放到build_Armv7目录
  wget -c https://dl.google.com/android/repository/android-ndk-r23b-linux.zip
fi
if [ ! -d "android-ndk-r23b" ];then
  unzip -q android-ndk-r23b-linux.zip
fi

export ANDROID_NDK=$PWD/android-ndk-r23b
export NDK_TOOLCHAIN=$ANDROID_NDK/toolchains/llvm/prebuilt/linux-x86_64

ccache -M 10G
ccache -s

# build sqlite3
echo "build sqlite..."
if [ ! -f "sqlite-autoconf-3370200.tar.gz" ];then
# 如果下载不下来，可以手动下载后放到build_Armv7目录
  wget -c https://sqlite.org/2022/sqlite-autoconf-3370200.tar.gz
fi
if [ ! -d "sqlite-autoconf-3370200" ];then
  tar xzf sqlite-autoconf-3370200.tar.gz
fi

cd sqlite-autoconf-3370200
CC="ccache $NDK_TOOLCHAIN/bin/armv7a-linux-androideabi22-clang" ./configure \
  --prefix=/tmp/gdal_ndk_arm --host=armv7a-linux-androideabi22
make -j16 
make install
cd ..

# Build proj
echo "build proj..."
if [ ! -f "proj-9.2.1.tar.gz" ];then
# 如果下载不下来，可以手动下载后放到build_Armv7目录
  wget -c https://download.osgeo.org/proj/proj-9.2.1.tar.gz
fi
if [ ! -d "proj-9.2.1" ];then
  tar xzf proj-9.2.1.tar.gz
fi

cd proj-9.2.1
if [ ! -d "build" ];then
  mkdir -p build
fi
cd build
# See later comment in GDAL build section about MAKE_FIND_ROOT_PATH_MODE_INCLUDE, CMAKE_FIND_ROOT_PATH_MODE_LIBRARY
cmake .. \
  -DUSE_CCACHE=ON \
  -DENABLE_TIFF=OFF -DENABLE_CURL=OFF -DBUILD_APPS=OFF -DBUILD_TESTING=OFF \
  -DCMAKE_INSTALL_PREFIX=/tmp/gdal_ndk_arm \
  -DCMAKE_SYSTEM_NAME=Android \
  -DCMAKE_ANDROID_NDK=$ANDROID_NDK \
  -DCMAKE_ANDROID_ARCH_ABI=armeabi-v7a \
  -DCMAKE_SYSTEM_VERSION=22 \
  "-DCMAKE_PREFIX_PATH=/tmp/gdal_ndk_arm;$NDK_TOOLCHAIN/sysroot/usr/" \
  -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=NEVER \
  -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=NEVER \
  -DEXE_SQLITE3=/usr/bin/sqlite3 
  # -DBUILD_LIBPROJ_SHARED=OFF  #编译静态库
make -j16 
make install
cd ../..

# Build GDAL
echo "build gdal..."

if [ ! -f "gdal-3.6.4.tar.gz" ];then
# 如果下载不下来，可以手动下载后放到build_Armv7目录
  wget -c https://download.osgeo.org/gdal/3.6.4/gdal-3.6.4.tar.gz
fi
if [ ! -d "gdal-3.6.4" ];then
  tar xzf gdal-3.6.4.tar.gz
fi
cd gdal-3.6.4
if [ ! -d "build_android_cmake" ];then
  mkdir -p build_android_cmake
fi
cd build_android_cmake
# PKG_CONFIG_LIBDIR, CMAKE_FIND_ROOT_PATH_MODE_INCLUDE, CMAKE_FIND_ROOT_PATH_MODE_LIBRARY, CMAKE_FIND_USE_CMAKE_SYSTEM_PATH
# are needed because we don't install dependencies (PROJ, SQLite3) in the NDK sysroot
# This is definitely not the most idiomatic way of proceeding...
# 如果googletest下载不下来，可以手动下载后放到..\build_android_cmake\autotest\cpp\googletest-download\googletest-prefix\src目录
# Gdal的版本必须大于3.5.2才新增-DBUILD_WITHOUT_64BIT_OFFSET参数
PKG_CONFIG_LIBDIR=/tmp/gdal_ndk_arm/lib/pkgconfig cmake .. \
 -DUSE_CCACHE=ON \
 -DCMAKE_INSTALL_PREFIX=/tmp/gdal_ndk_arm \
 -DCMAKE_SYSTEM_NAME=Android \
 -DCMAKE_ANDROID_NDK=$ANDROID_NDK \
 -DCMAKE_ANDROID_ARCH_ABI=armeabi-v7a \
 -DCMAKE_SYSTEM_VERSION=22 \
 "-DCMAKE_PREFIX_PATH=/tmp/gdal_ndk_arm;$NDK_TOOLCHAIN/sysroot/usr/" \
 -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=NEVER \
 -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=NEVER \
 -DCMAKE_FIND_USE_CMAKE_SYSTEM_PATH=NO \
 -DSFCGAL_CONFIG=disabled \
 -DHDF5_C_COMPILER_EXECUTABLE=disabled \
 -DHDF5_CXX_COMPILER_EXECUTABLE=disabled \
 -DBUILD_WITHOUT_64BIT_OFFSET=YES 

make -j16 
make install
cd ../..

echo "finish."
